function drawChart() {
    const chartCtx = document.getElementById('chart').getContext('2d');

    const chartOptions = {
        type: 'line',
        data: {
            labels: [1, 0.7, 0.5, 0.3],
            datasets: [{
                label: 'Apriori',
                backgroundColor: '#3F51B5',
                borderColor: '#3F51B5',
                fill: false,
                data: [249, 821, 1235, 1993]
            }, {
                label: 'Eclat',
                backgroundColor: 'red',
                borderColor: 'red',
                fill: false,
                data: [7, 12, 27, 411]
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Min Support'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Time (seconds)'
                    }
                }]
            }
        }
    };

    const myChart = new Chart(chartCtx, chartOptions);

    (function fitChart() {
        const xAxisLabelMinWidth = 10;

        const chartCanvas = document.getElementById('chart');
        const maxWidth = chartCanvas.parentElement.parentElement.clientWidth;
        const width = Math.max(myChart.data.labels.length * xAxisLabelMinWidth, maxWidth);

        chartCanvas.parentElement.style.width = width + 'px';
    })();
}

drawChart();